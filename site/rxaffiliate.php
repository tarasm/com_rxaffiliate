<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//require_once JPATH_COMPONENT . '/helpers/route.php';
require_once( __DIR__ . '/helpers/client_xmlrpc.php' );
require_once( __DIR__ . '/helpers/helper.php');

$controller = JControllerLegacy::getInstance('Rxaffiliate');

$shopClose = xmlrpcClient::call(xmlrpcClient::IS_SHOP_CLOSED);
if(strtolower($shopClose) == 'y'){
	JFactory::getApplication()->input->set("view","shopclose");
	$controller->execute("");
}else{
	$controller->execute(JFactory::getApplication()->input->get('task'));
}

$controller->redirect();
