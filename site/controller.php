<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/exception.php");

class RxaffiliateController extends JControllerLegacy
{
	// Set the default view name and format from the Request.
	protected $default_view = "main";

	public static $models;

	/**
	 * Method to display a view.
	 *
	 * @param   boolean			If true, the view output will be cached
	 * @param   array  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false){

		try{
			$model = $this->getModel($this->input->getString("view",null));

			switch($this->input->getString("view","")){
				case "category":
					/** @var RxaffiliateModelCategory $model */
				    //$model->set("category_id",$this->input->getInt("id",0));
					if(!$model->setCategory( $this->input->getInt("id",0))){
						throw new inputParameterException("");
					};
					break;

				case "productlist":
					/** @var RxaffiliateModelProductlist $model */
					$model = $this->getModel("Productlist");

					switch($this->input->getWord("type")){
						case "bestseller":
							$model->setBestsellers();
							break;

						case "featured":
							/** @var RxaffiliateModelProductlist $model */
							$model->setFeatured();
							break;

						default:
							throw new inputParameterException("");
					}
					break;
				case "rendermodule":
					$model->set("modulename",$this->input->getCmd("name",""));
					$model->set("moduletitle", $this->input->getString("title"));
					break;
				case "product":
				case "pages":
					/** @var RxaffiliateModelProduct|RxaffiliateModelPages $model */
					$model->set("id",$this->input->getInt("id",0));
					break;
			}

			$safeurlparams = array('catid' => 'INT', 'id' => 'INT', 'cid' => 'ARRAY', 'year' => 'INT', 'month' => 'INT', 'limit' => 'UINT', 'limitstart' => 'UINT',
				'showall' => 'INT', 'return' => 'BASE64', 'filter' => 'STRING', 'filter_order' => 'CMD', 'filter_order_Dir' => 'CMD', 'filter-search' => 'STRING', 'print' => 'BOOLEAN', 'lang' => 'CMD');

			parent::display($cachable, $safeurlparams);
		}catch (inputParameterException $e){
			$this->redirectErrorPage(404,JText::_("COM_RXAFFILIATE_ERROR_INPUT_DATA"));
		}

		return $this;
	}

	public function searchfirst(){
		/** @var RxaffiliateModelSearch $search */
		$search = $this->getModel("Search")
			 ->setLetter($this->input->getWord("srch",""));

		$this->displaySearch($search->getCategory());
	}

	public function search(){
		/** @var RxaffiliateModelSearch $search */
		$search = $this->getModel("Search")
			 ->setSrch($this->input->getString("srch",""),$this->input->getInt("page",1));
		$this->displaySearch($search->getCategory());
	}

	/**
	 * @param RxaffiliateTemplateCategory $data
	 */
	private function displaySearch($data){
		$this->input->set("view","category");

		/** @var RxaffiliateModelCategory $model */
		$model = $this->getModel("category");
		$model->setCategory($data);

		parent::display();
	}

	public function addproduct(){
		try{
			$quantity = $this->input->getInt("quantity",1);
			if($quantity < 1){ throw new inputParameterException("parameter \"quantity\" not valid"); };

			$product_id = $this->input->getInt("id",0);
			if($product_id < 1){ throw new inputParameterException("parameter \"id\" (product) not valid"); };

			$cart = $this->getModel("cart");
			/** @var RxaffiliateModelCart $cart */
			$cart->addProduct($product_id,$quantity);

		}catch(inputParameterException $e){

		}

		$this->redirectCart();
	}

	public function setquantity(){
		try{
			$quantity = $this->input->getInt("quantity",1);
			if($quantity < 1){ throw new inputParameterException("parameter \"quantity\" not valid"); };

			$product_id = $this->input->getInt("id",0);
			if($product_id < 1){ throw new inputParameterException("parameter \"id\" (product) not valid"); };

			$cart = $this->getModel("cart");
			/** @var RxaffiliateModelCart $cart */
			$cart->setProductQuantity($product_id,$quantity);

		}catch(inputParameterException $e){

		}

		$this->redirectCart();
	}

	public function clear(){
		try{
			$product_id = $this->input->getInt("id",0);

			$cart = $this->getModel("cart");
			/** @var RxaffiliateModelCart $cart */
			$cart->clear($product_id);
		}
		catch(inputParameterException $e){

		}
		$this->redirectCart();
	}

	protected function redirectCart(){
		$uri = JUri::getInstance();
		$uri->delVar("task");
		$uri->delVar("id");
		$uri->delVar("quantity");
		$uri->setVar("view",$this->input->getString("view","cart"));

		$this->setRedirect($uri->toString());
	}

	public function getModel($name = '', $prefix = '', $config = array()) {
		$model = parent::getModel($name, $prefix, $config);
		$class = get_class($model);
		$self_class = get_class($this);
		if(!empty($self_class::$models[$class])){
			return $self_class::$models[$class];
		}else{
			$self_class::$models[$class] = $model;
			return $model;
		}
	}

	private function redirectErrorPage($code,$message){
		$uri = JUri::getInstance();
		$uri->delVar("task");
		$uri->setVar("view","error");

		/** @var RxaffiliateModelError $model */
		$model = $this->getModel("Error");
		$model->setError($code,$message);

		$this->setRedirect($uri->toString());
	}
}