<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/../view.html.php");

class RxaffiliateViewRendermodule extends RxaffiliateView
{
    /** @var  string */
	public $content;

	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		/** @var RxaffiliateModelRendermodule $model */
		$model = $this->getModel();

		$module = JModuleHelper::getModule($model->modulename,$model->moduletitle);

		if($this->content = JModuleHelper::renderModule($module)){
			JFactory::getApplication()->input->set("tmpl","component");
			parent::display($tpl);
		}
	}

}
