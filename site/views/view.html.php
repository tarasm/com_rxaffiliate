<?php
/**
 * User: Taras
 * Date: 09.01.15
 * Time: 12:50
 */
  class RxaffiliateView extends JViewLegacy {
	  public $title;

	  /** @var  JDocumentHTML */
	  public $document;

	  public function __construct($config = array()){
		  JHtml::stylesheet("rxaffiliate/style.css",array(),true);
		  JHtml::_('bootstrap.framework');


		  parent::__construct($config);
	  }

	  public function display($tpl = null) {
		  if($this->title){
			  $this->document->setTitle($this->title);
		  }

		  return parent::display($tpl);
	  }


  }