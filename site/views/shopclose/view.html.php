<?php

defined('_JEXEC') or die;

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
class RxaffiliateViewShopclose extends RxaffiliateView
{
	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		parent::display($tpl);
	}

}
