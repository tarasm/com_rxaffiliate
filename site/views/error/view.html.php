<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/../view.html.php");

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
class RxaffiliateViewError extends RxaffiliateView
{
	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		/** @var RxaffiliateModelError $model */
		$model = $this->getModel();

		JError::raiseWarning($model->code, $model->message);

		parent::display($tpl);
	}

}
