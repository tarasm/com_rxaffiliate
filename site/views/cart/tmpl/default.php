<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/** @var RxaffiliateViewCart $this */

defined('_JEXEC') or die; ?>
<div class="rxaffiliate-cart">
	<h3 class="rxaffiliate-cart-title"><?php echo(JText::_("COM_RXAFFILIATE_CART_TITLE")); ?></h3>
	<p class="rxaffiliate-cart-shipping-note"><?php
	    printf( JText::_("COM_RXAFFILIATE_CART_SHIPPING_NOTE"),
			    RxaffiliateHelper::formatPrice($this->shipping_term)); ?></p>

	<table class="rxaffiliate-products">
		<thead>
			<th><?php echo(JText::_("COM_RXAFFILIATE_NAME")); ?></th>
			<th><?php echo(JText::_("COM_RXAFFILIATE_SET")); ?></th>
			<th><?php echo(JText::_("COM_RXAFFILIATE_WEIGHT")); ?></th>
			<th><?php echo(JText::_("COM_RXAFFILIATE_PRICE")); ?></th>
			<th>&nbsp;</th>
		</thead>
		<tbody>
<?php       foreach($this->products as $product){ ?>
           <tr>
			   <td><a href="<?php RxaffiliateHelper::makeUrl(array(
					   "view" => "product", "id" => $product->id )); ?>"><?php
					      echo($product->title); ?></a></td>
			   <td><?php echo($product->quantity); ?></td>
			   <td><?php echo($product->weight); ?></td>
			   <td><?php RxaffiliateHelper::printPrice($product->price); ?></td>
			   <td><form action="<?php RxaffiliateHelper::makeUrl(array(
					       "task" => "setquantity" )); ?>" method="POST">
					   <input type="hidden" name="id" value="<?php echo($product->id); ?>" />
				       <input type="text" name="quantity" value="<?php echo($product->count); ?>" size="4" />
			           <input type="submit" class="btn" value="<?php
	                            echo(JText::_("COM_RXAFFILIATE_UPDATE")); ?>" />
			        </form>
			        <a href="<?php RxaffiliateHelper::makeUrl(array(
						"task" => "clear", "id" => $product->id	)); ?>" class="btn"><?php
						     echo(JText::_("COM_RXAFFILIATE_CLEAR_CART")); ?></a> </td>
           </tr>

<?php	   } ?>
         </tbody>
    </table>

	<ul class="rxaffiliate-cart-total">
		<li><a href="<?php RxaffiliateHelper::makeUrl(array(
				"task" => "clear" )); ?>"><?php
				echo(JText::_("COM_RXAFFILIATE_CLEAR_CART")); ?></a></li>
		<li><?php echo(JText::_("COM_RXAFFILIATE_SUBTOTAL")); ?>: <span><?php
				RxaffiliateHelper::printPrice($this->subtotal); ?></span></li>
		<li><?php echo(JText::_("COM_RXAFFILIATE_SHIPPING")); ?>: <span><?php
				RxaffiliateHelper::printPrice($this->shipping); ?></span></li>
<?php if($this->discount > 0){ ?>
		<li><?php echo(JText::_("COM_RXAFFILIATE_DISCOUNT")); ?>: <span><?php
				RxaffiliateHelper::printPrice($this->discount); ?></span></li>
<?php } ?>
		<li><?php echo(JText::_("COM_RXAFFILIATE_TOTAL")); ?>: <span><?php
				RxaffiliateHelper::printPrice($this->total); ?></span></li>
		<li><?php echo($this->checkout_form); ?></li>
	</ul>
</div>