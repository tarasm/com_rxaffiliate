<?php
/**
 * User: Taras
 * Date: 21.01.15
 * Time: 16:25
 */

/** @var RxaffiliateViewCart $this */
defined('_JEXEC') or die; ?>
<form name="checkout" action="<?php echo($this->checkout_action); ?>" method="POST">
<?php foreach($this->products as $product){ ?>
		<input type="hidden" name="id<?php echo($product->id); ?>" value="<?php echo($product->count); ?>" />
<?php } ?>

<?php foreach($this->checkout_params as $p_name => $p_value){ ?>
		<input type="hidden" name="<?php echo($p_name); ?>" value="<?php echo($p_value); ?>" />
<?php } ?>
	<input type="submit" value="<?php echo(JText::_("COM_RXAFFILIATE_CHECKOUT")); ?>" class="btn" />
</form>