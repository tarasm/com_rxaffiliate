<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/../view.html.php");
/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
class RxaffiliateViewCart extends RxaffiliateView{
	public $shipping_price;
	public $shipping_term;
	public $title;
	public $checkout_action;
	public $checkout_params = array();
	public $checkout_form;

	public $currency;

	public $shipping;
	public $total;
	public $discount;
	public $subtotal;

	/** @var  RxaffiliateTemplateCartproduct[] */
	public $products;

	public function __construct($config = array()){
		parent::__construct($config);

		$shipping = JModelLegacy::getInstance("Shipping", "RxaffiliateModel");
		/** @var RxaffiliateModelShipping $shipping */
		$this->shipping_price = $shipping->price;
		$this->shipping_term = $shipping->term;

		$this->title = RxaffiliateHelper::getTitleFromActive();
	}


	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null){

		$model = $this->getModel();
		/** @var RxaffiliateModelCart $model */
		$this->products = $model->products;

		$this->currency = $model->getCurrency();

		if(count($this->products) > 0){
			$this->makeCheckoutParams();
			$this->checkout_form = $this->loadTemplate("checkout");

			$this->makeTotalParam();
			parent::display($tpl);
		}else{
			parent::display("empty");
		}
	}

	private function makeCheckoutParams(){
		$params = JComponentHelper::getParams('com_rxaffiliate');

		$success_url = clone JUri::getInstance();
		$success_url->parse(JRoute::_("index.php?option=com_rxaffiliate&task=success"));

		foreach(array('Scheme', 'User', 'Pass', 'Host', 'Port', 'Path', 'Query', 'Fragment') as $part){
			$getMethod = "get" . $part;
			$setMethod = "set" . $part;
			if($success_url->$getMethod()){
				break;
			}

			$success_url->$setMethod(JUri::getInstance()->$getMethod());
		}
		$this->checkout_params = array(
			 "mode" => "massadd"
			,"success_url" => $success_url->toString()
			,"partner" => $params->partner
			,"uniqueid" => $params->unique_id
			,"provider" => JUri::getInstance()->getHost()
		);

		$params = JComponentHelper::getParams('com_rxaffiliate');
		$this->checkout_action = $params->get("checkoutaction");
	}

	private function makeTotalParam(){
		/** @var RxaffiliateModelCart $model */
		$model = $this->getModel()->calculate();
		$this->shipping = $model->shipping;
		$this->discount = $model->discount;
		$this->subtotal = $model->subtotal;
		$this->total = $model->total;
	}

}
