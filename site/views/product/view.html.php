<?php

defined('_JEXEC') or die;

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */

include_once(__DIR__ . "/../view.html.php");
class RxaffiliateViewProduct extends RxaffiliateView{

	public $id;
	public $meta_description;
    public $location_category_id;
    public $location_category_title;
    public $location_subcat_id;
	public $location_subcat_title;
	public $meta_keywords;
	public $title;
	public $product_description;
	public $product_weight;
	public $product_set;
	public $product_perpill;
	public $product_price;
	public $product_image_url;
	public $product_image_height;
	public $product_image_width;
	public $currency;

	/**
	 * @var JDocumentHTML $document
	 */

	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		$product = $this->getModel();
		/**
		 * @var RxaffiliateModelProduct $product
		 */

		$this->currency = $product->getCurrency();

		foreach(array_keys( $this->getProperties() ) as $property){
			if(isset($product[$property])){
				$this->$property = $product[$property];
			}
		}

		$document = $this->document;
		/**
		 * @var JDocumentHTML $document
		 */
		$document->setDescription($this->meta_description);
		$document->setTitle($this->title);
		$document->setMetaData("keywords",$this->meta_keywords);

		parent::display($tpl);
	}

}
