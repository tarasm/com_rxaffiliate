<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * @var RxaffiliateViewProduct $this
 */

defined('_JEXEC') or die; ?>

<div class="rxaffiliate-product">
    <h3 class="rxaffiliate-product"><?php echo($this->title); ?></h3>
	<img src="<?php echo($this->product_image_url); ?>" alt="<?php echo($this->title); ?>" class="rxaffiliate-product"/>
	<table class="rxaffiliate-products rxaffiliate-product">
		<thead>
			<th><?php echo(JText::_("COM_RXAFFILIATE_SET")); ?></th>
			<th><?php echo(JText::_("COM_RXAFFILIATE_WEIGHT")); ?></th>
			<th><?php echo(JText::_("COM_RXAFFILIATE_PRICE")); ?></th>
			<th>&nbsp;</th>
		</thead>
		<tbody>
			<tr>
				<td><?php echo($this->product_set); ?></td>
				<td><?php echo($this->product_weight); ?></td>
				<td><?php echo($this->product_price); ?></td>
				<td><a href="<?php RxaffiliateHelper::makeUrl(array(
						"task" => "addproduct", "id" => $this->id
					               )); ?>"><?php echo(JText::_("COM_RXAFFILIATE_ADDCART")); ?></a></td>
			</tr>
		</tbody>
	</table>

	<div class="rxaffiliate-product-info">
		<h3><?php echo(JText::_("COM_RXAFFILIATE_GENERAL_DESCRIPTION")); ?></h3>
		<div class="rxaffiliate-product-description"><?php echo($this->product_description); ?></div>
	</div>
</div>
