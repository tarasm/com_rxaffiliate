<?php
/**
 * User: Taras
 * Date: 18.01.15
 * Time: 12:03
 */

/** @var RxaffiliateViewCategory $this */

defined('_JEXEC') or die; ?>
<tr>
	<td><a href="<?php RxaffiliateHelper::makeUrl(array(
	        "view" => "product", "id" => $this->id
            )); ?>"><?php echo($this->title); ?></a></td>
	<td><?php echo($this->quantity); ?></td>
	<td><?php echo($this->weight); ?></td>
	<td><?php  RxaffiliateHelper::printPrice($this->price); ?></td>
	<td><a href="<?php RxaffiliateHelper::makeUrl(array(
			"task" => "addproduct", "id" => $this->id
		)); ?>"><?php echo(JText::_("COM_RXAFFILIATE_ADDCART")); ?></a></td>
</tr>
