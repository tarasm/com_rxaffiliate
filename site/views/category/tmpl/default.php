<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/** @var RxaffiliateViewCategory $this */

defined('_JEXEC') or die; ?>

<div class="rxaffiliate-category">
  <h3 class="rxaffiliate-category-title"><?php echo($this->title); ?></h3>

  <p class="rxaffiliate-category-desc"><?php  echo($this->description); ?></p>

<?php foreach( $this->subcategories as $subcategory){
		echo($subcategory);
      } ?>

<?php if(count($this->products) > 0){ ?>
	<table class="rxaffiliate-products">
		<thead><?php echo($this->loadTemplate("productheader")); ?></thead>
		<tbody>
		<?php   foreach($this->products as $product){
			echo($product);
		} ?>
		</tbody>
	</table>
<?php } ?>

</div>