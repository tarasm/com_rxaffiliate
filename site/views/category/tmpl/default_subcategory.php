<?php
/**
 * User: Taras
 * Date: 18.01.15
 * Time: 12:46
 */
/** @var RxaffiliateViewCategory $this */

defined('_JEXEC') or die; ?>

<div class="rxaffiliate-subcategory">
	<h3><a href="<?php RxaffiliateHelper::makeUrl(array(
				"view" => "category", "id" => $this->id
			)); ?>"><?php echo($this->title); ?></a></h3>

    <table class="rxaffiliate-products">
	  <thead><?php echo($this->loadTemplate("productheader")); ?></thead>
	  <tbody>
<?php   foreach($this->products as $product){
			echo($product);
        } ?>
	  </tbody>
    </table>
</div>
