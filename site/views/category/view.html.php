<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/../view.html.php");
/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
class RxaffiliateViewCategory extends RxaffiliateView {
	/** @var  int */
	public $id;

	/** @var  string */
	public $title;

	/** @var  string[] */
	public $subcategories = array();

	/** @var  string */
	public $description;

	/** @var  string */
	public $full_description;

	/** @var  string[] */
	public $products =array();

	/** @var  string
	 * for product */
	public $quantity;

	/** @var  string|int
	 *  for product */
	public $price;

	/** @var  string
	 *  for product */
	public $weight;

	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null){
		$category = $this->get("category");
		/** @var RxaffiliateTemplateCategory $category */
		if(!$category){ throw new inputParameterException(""); }

		$this->subcategories = $this->loadSubcategories($category->subcategories);
		$this->products = $this->loadProducts($category->products);

		$this->id = $category->id;
		$this->title = $category->title;
		$this->description = $category->description;
		$this->full_description = $category->full_description;

		$this->document->setTitle($category->title);
		$this->document->setDescription($category->meta_description);
		$this->document->setMetaData("keywords",$category->meta_keywords);

		parent::display($tpl);
	}

	/**
	 * @param RxaffiliateTemplateProduct[] $products
	 * @return string[]
	 */
	private function loadProducts( $products){
		$result = array();

		foreach($products as $product){
		    $this->id = $product->id;
		    $this->title = $product->title;
		    $this->price = $product->price;
		    $this->quantity = $product->quantity;
		    $this->weight = $product->weight;

		    $result[] = $this->loadTemplate("product");
		}
		return $result;
	}

	/**
	 * @param RxaffiliateTemplateCategory[] $subcategories
	 * @return string[]
	 */
	private function loadSubcategories(array $subcategories){
		$result = array();
		foreach($subcategories as $subcategory){
			//$this->subcategories = $this->loadSubcategories($subcategory->subcategories);

			$this->products = $this->loadProducts($subcategory->products);

			$this->id = $subcategory->id;
			$this->title = $subcategory->title;

			if(count($subcategory->products) > 0){
				$result[] = $this->loadTemplate("subcategory");
			}else{
				$result[] = $this->loadTemplate("subcategorynoproduct");
			}
		}
		return $result;
	}
}
