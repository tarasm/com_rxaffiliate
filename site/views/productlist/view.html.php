<?php

defined('_JEXEC') or die;

include_once(__DIR__ . "/../view.html.php");

class RxaffiliateViewProductlist extends RxaffiliateView
{
	/** @var RxaffiliateTemplateProduct[] */
	public $products = array();

	/** @var  string */
	public $title;

	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		/** @var RxaffiliateModelProductlist $model */
		$model = $this->getModel();
		$this->products = $model->getProducts();

		try {
			$active = JFactory::getApplication()->getMenu()->getActive();
			$this->title = $active->title;
		}catch (Exception $e){ };

		parent::display($tpl);
	}

}
