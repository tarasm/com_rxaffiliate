<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/** @var RxaffiliateViewProductlist $this */

defined('_JEXEC') or die;
?>
<h2><?php echo($this->title); ?></h2>
<ul class="rxaffiliate-products">
<?php foreach($this->products as $product){ ?>
		<li class="rxaffiliate-products-item">
			<img src="<?php echo( RxaffiliateHelper::productImageUrl($product->imageUrl) );
			?>" height="<?php echo( $product->imageHeight );
			?>" width="<?php  echo( $product->imageWidth);
			?>" alt="<?php    echo( $product->title ); ?>" />
			<h3>
				<a href="<?php RxaffiliateHelper::makeUrl(array(
					"view" => "product", "id" => $product->id )) ?>"><?php
					echo($product->title); ?></a>
			</h3>

			<span class="prof"><?php echo( $product->weight ); ?>/<?php
				  echo( $product->quantity); ?></span>
			<span class="price"><?php echo( RxaffiliateHelper::formatPrice($product->price) ); ?></span>
			<a href="<?php RxaffiliateHelper::makeUrl(array(
				"task" => "addtocart", "id" => $product->id	)) ?>" class="btn"><?php
				echo( JText::_("COM_RXAFFILIATE_ADDCART")); ?></a>
		</li>

<?php } ?>
</ul>