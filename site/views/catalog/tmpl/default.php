<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/** @var RxaffiliateViewCatalog $this */

defined('_JEXEC') or die; ?>
<div class="rxaffiliate-catalog">
	<h2 class="rxaffiliate-catalog-title"><?php echo($this->title); ?></h2>
<?php foreach($this->catalog as $catalog){ ?>
		<div class="rxaffiliate-catalog-category">
			<a class="rxaffiliate-catalog-category-title" href="<?php RxaffiliateHelper::makeUrl(array(
				"view" => "category", "id" => $catalog->id )); ?>"><?php
				       print $catalog->title; ?></a>
			<div class="rxaffiliate-catalog-subcategory">
			  <?php foreach($catalog->subcategories as $subcategory){ ?>
				<a href="<?php RxaffiliateHelper::makeUrl(array(
					"view" => "category",
					"id" => $subcategory->id
				)); ?>" class="rxaffiliate-catalog-subcategory-title"><?php
					print $subcategory->title; ?></a>
			  <?php } ?>
			</div>
		</div>
<?php } ?>
</div>
