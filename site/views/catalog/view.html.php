<?php

defined('_JEXEC') or die;

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
include_once(__DIR__ . "/../view.html.php");

class RxaffiliateViewCatalog extends RxaffiliateView{
	/** @var  RxaffiliateTemplateCategory[] */
	public $catalog;
	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null){
		$this->title = JText::_("COM_RXAFFILIATE_CATALOG_TITLE");

		/** @var RxaffiliateModelCatlog $model */
		$model = $this->getModel();
		$this->catalog = $model->getCatalog();

		parent::display($tpl);
	}

}
