<?php

defined('_JEXEC') or die;

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
class RxaffiliateViewMain extends JViewLegacy
{
	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null)
	{
		$livepath = JUri::root();
		$livepath = preg_replace("/\\/\$/","", $livepath);

		$this->assign("livepath",$livepath);

		$doc = JFactory::getDocument();
//		$doc->addScript($livepath . "/media/rxaffiliate/js/webcam.min.js");
		JHtml::_("jquery.framework");
//		JHtml::script("rxaffiliate/webcam.js", false, true);
//		JHtml::script("rxaffiliate/rxaffiliate_main.js", false, true);
 

		parent::display($tpl);
	}

}
