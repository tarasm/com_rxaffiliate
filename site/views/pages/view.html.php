<?php

defined('_JEXEC') or die;

/**
 * Frontpage View class
 *
 * @package     Joomla.Site
 * @subpackage  com_contact
 * @since       1.6
 */
include_once(__DIR__ . "/../view.html.php");
class RxaffiliateViewPages extends RxaffiliateView{
	public $page;
	public $title;

	/**
	 * Display the view
	 *
	 * @return  mixed  False on error, null otherwise.
	 */
	public function display($tpl = null){
		$input = JFactory::getApplication()->input;
		$model = $this->getModel();
		/**
		 * @var RxaffiliateModelPages $model
		 */
		$this->page = $model->getPage();

		try {
			$active = JFactory::getApplication()->getMenu()->getActive();
			$this->title = $active->title;
		}catch (Exception $e){ };

		parent::display($tpl);
	}
}
