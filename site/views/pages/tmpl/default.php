<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>
<div class="rxaffiliate-page">
	<h3 class="rxaffiliate-page-title"><?php echo($this->title); ?></h3>
<?php echo($this->page); ?>
</div>