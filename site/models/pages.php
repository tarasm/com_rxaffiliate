<?php
/**
 * User: Taras
 * Date: 10.01.15
 * Time: 3:59
 */
include_once(realpath( __DIR__ . "/../model.php"));
require_once(realpath( __DIR__ . '/../helpers/client_xmlrpc.php' ));


class RxaffiliateModelPages extends RxaffiliateModel{
	/** @var  int */
	public $id;

	public function getPage($id = null){
		if(!$id) $id = $this->id;

		if($id){
			$result = xmlrpcClient::call(xmlrpcClient::GET_PAGES,array($id));
			return $result;
		}
	}
}
