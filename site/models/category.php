<?php
/**
 * User: taras
 * Date: 06.10.14
 * Time: 0:31
 */

include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelCategory extends RxaffiliateModel {

	/** @var  RxaffiliateTemplateCategory */
	protected $category;

	/**
	 * @param int|RxaffiliateTemplateCategory $id
	 * @return RxaffiliateTemplateCategory|null|void
	 * @throws inputParameterException
	 */
	public function setCategory($id) {
		if(!empty($id) && is_int($id)){
			$this->category_id = $id;

			$result = parent::getCategory();

			if(is_array($result)){
				$this->category = $this->parseCategory1($result,$this->category_id);
			}
			elseif(is_array( $result = $this->getSubcategory())){
				$this->category = $this->parseCategory2($result,$this->category_id);
			}

			return $this->category;
		}elseif(is_object($id) && (is_subclass_of($id,"RxaffiliateTemplateCategory")
								|| (get_class($id) == "RxaffiliateTemplateCategory"))){
			$this->category = $id;
		}
		else{
			throw new inputParameterException("");
		}
	}

	/**
	 * @return null|RxaffiliateTemplateCategory
	 */
	public function getCategory(){
		return $this->category;
	}

	/**
	 * @param array $data
	 * @param int $id
	 *
	 * @return RxaffiliateTemplateCategory
	 */
	protected function parseCategory1($data,$id){
		if(count($data) < 1){
			return null;
		}

		$result = new RxaffiliateTemplateCategory(array());
		$result->id = $id;
		$result->title = $data[0]["title"];
		$result->description = $data[0]["cat_description"];
		$result->meta_description = $data[0]["meta_description"];
		$result->meta_keywords = $data[0]["meta_keywords"];

		foreach($data as $category){
			if(($subCategoryKey = $this->findSubcategories($result->subcategories,
				                                      (int) ($category["product_cat_id"]))) === false){
				$subcategory = new RxaffiliateTemplateCategory();
				$subcategory->id = $category["product_cat_id"];
				$subcategory->title = $category["subcategory_title"];

				$subCategoryKey = count($result->subcategories);
				$result->subcategories[$subCategoryKey] = $subcategory;
			}

			$product = new RxaffiliateTemplateProduct();
			$product->id = $category["product_id"];
			$product->title = $category["product_title"];
			$product->price = $category["product_price"];
			$product->quantity = $category["product_quantity"];
			$product->weight = $category["product_weight"];

			$result->subcategories[$subCategoryKey]->products[] = $product;
		}
		return $result;
	}

	/**
	 * @param array $data
	 * @param int $id
	 * @return RxaffiliateTemplateCategory
	 */
	protected function parseCategory2($data,$id){
		if(count($data) < 1){
			return null;
		}

		$result = new RxaffiliateTemplateCategory();
		$result->id = $id;
		$result->meta_description = $data[0]["meta_description"];
		$result->meta_keywords = $data[0]["meta_keywords"];
		$result->title = $data[0]["title"];
		$result->description = $data[0]["general_desc"];
		$result->full_description = $data[0]["full_desc"];

		$result->parent = new RxaffiliateTemplateCategory(
			array(  "id" => $data[0]["location_id"],
		         "title" => $data[0]["location_title"]));

		foreach($data as $curr_product){
			$product = new RxaffiliateTemplateProduct();
			$product->id = $curr_product["product_id"];
			$product->title = $curr_product["product_title"];
			$product->quantity = $curr_product["product_quantity"];
			$product->weight = $curr_product["product_weight"];
			$product->price = $curr_product["product_price"];

			$result->products[] = $product;
		}

		return $result;
	}

	/**
	 * @param RxaffiliateTemplateCategory[] $categories
	 * @param int $id
	 *
	 * @return int|bool
	 */
	private function findSubcategories(array $categories, $id){
		foreach($categories as $key => $category){
			if($category->id == $id){
				return $key;
			}
		}
		return false;
	}


}