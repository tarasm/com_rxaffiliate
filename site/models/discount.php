<?php
/**
 * User: Taras
 * Date: 21.01.15
 * Time: 18:45
 */
include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelDiscount extends RxaffiliateLibCarthelpers {
	/** @var  float */
	private $shipping;

	/**
	 * @return float
	 *
	 * calculate value
	 */
	public function getVal() {
		return 0;
	}

	/**
	 * @param float $shipping
	 * @return self $this
	 */
	public function setShipping($shipping){
		$this->shipping = $shipping;
		return $this;
	}
}