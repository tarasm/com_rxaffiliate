<?php
/**
 * User: Taras
 * Date: 15.02.15
 * Time: 14:22
 */
include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelRendermodule extends RxaffiliateModel {
	/** @var  string */
	public $modulename;

	/** @var  string */
	public $moduletitle;
}