<?php
/**
 * User: taras
 * Date: 22.11.14
 * Time: 14:40
 */

  include_once(realpath( __DIR__ . "/../model.php"));

  class RxaffiliateModelCart extends RxaffiliateModel implements IteratorAggregate {
	  protected $session;

	  /**
	   * Products list
	   * @var RxaffiliateTemplateCartproduct[]
	   */
	  public $products = array();

	  /** @var  float */
	  public $shipping;

	  /** @var  float */
	  public $discount;

	  /** @var  float */
	  public $subtotal;

	  /** @var  float */
	  public $total;

	  function __construct($config = array()){
	      parent::__construct($config);

		  $this->session = JFactory::getSession();

		  $products = $this->session->get("cart","");
		  $products = @json_decode($products);

		  if(is_array($products)){
			  foreach($products as $product){
				  $this->products[] = new RxaffiliateTemplateCartproduct($product);
			  }
		  }
	  }

	  private function store(){
	      $storedVars = array();
		  foreach($this->products as $product){
			  $storedVars[] = $product->toArray();
		  }

		  $this->session->set("cart", json_encode($storedVars));

		  $this->total = $this->subtotal = null;
	  }

	  public function getIterator(){
		  return new ArrayIterator($this->products);
	  }

	  /**
	   * @param int $id productid
	   * @param int $quantity
	   */
      function addProduct($id, $count = 1){
		  if(is_object($id)){
			  $id = $id->id;
		  }

		  $product = $this->getProductFromId($id);
		  $product->count += (int) $count;

		  $this->store();
		  return $this;
	  }

	  function setProductQuantity($id,$quantity = 0){
		  if(is_object($id)){
			  $id = $id->id;
		  }

		  $product = $this->getProductFromId($id);

		  if($quantity > 0){
			  $product->count = (int) $quantity;
		  }else{
			  return $this->clear($id);
		  }
		  $this->store();
		  return $this;
	  }

	  /**
	   * Clear cart
	   * @param int $id is set clear product from $id else clear all products
	   */
	  function clear($id = 0){
		  if($id > 0){
			  foreach($this->products as $key => $product){
				  if($id == $product->id){
					  array_splice($this->products,$key,1,array());
					  break;
				  }
			  }
		  }else{
		      $this->products = array();
		  }
		  $this->store();
		  return $this;
	  }

	  private function getProductFromId($id){
		  foreach($this->products as $product){
			  if($id == $product->id){
				  return $product;
			  }
		  }

		  $product = $this->getProduct($id);
		  $result = new RxaffiliateTemplateCartproduct();
		  $result->id = $product->product_id;
		  $result->title = $product->product_title;
		  $result->quantity = $product->product_value;
		  $result->weight = $product->product_weight;
		  $result->price = $product->product_price;
		  $result->count = 0;

		  $this->products[] = $result;
		  return $result;
	  }

	  public function calculate(){
		  if($this->total){
			  return $this;
		  }

		  /** @var RxaffiliateModelShipping $shipping */
		  $shipping = JModelLegacy::getInstance("Shipping","RxaffiliateModel");
		  $this->shipping = $shipping->setProduct($this->products)->getVal();

		  /** @var RxaffiliateModelDiscount $discount */
		  $discount = JModelLegacy::getInstance("Discount","RxaffiliateModel");
		  $this->discount = $discount->setProduct($this->products)
			                	->setShipping($this->shipping)->getVal();

		  $this->subtotal = 0;
		  foreach($this->products as $product){
			  $this->subtotal += $product->price * $product->count;
		  }

		  $this->total = $this->subtotal + $this->shipping - $this->discount;

		  return $this;
	  }
  }