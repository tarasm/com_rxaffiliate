<?php
/**
 * User: Taras
 * Date: 21.01.15
 * Time: 18:48
 */

include_once __DIR__ . "/../../model.php";

abstract class RxaffiliateLibCarthelpers extends RxaffiliateModel{

	/** @var  RxaffiliateTemplateProduct[] */
	protected $products = array();

	/**
	 * @param array|RxaffiliateTemplateProduct $product
	 * @return self
	 */
	public function setProduct($product){
		if(is_array($product)){
			$this->products = array();
		}else{
			$this->products[] = $product;
		}
		return $this;
	}

	/**
	 * @return int
	 *
	 * calculate value
	 */
	public abstract function getVal();
}