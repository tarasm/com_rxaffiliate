<?php
/**
 * User: Taras
 * Date: 07.01.15
 * Time: 15:26
 */

include_once(realpath( __DIR__ . "/category.php"));

class RxaffiliateModelSearch extends RxaffiliateModelCategory {
	/** @var  RxaffiliateTemplateCategory */
	protected $category;

	public function setLetter($letter){

		$result = $this->getSearchFirst($letter);
		if(is_array($result) && (count($result) > 0)){
			$result[0]["title"] = "";
			$result[0]["cat_description"] = "";
			$result[0]["meta_description"] = "";
			$result[0]["meta_keywords"] = "";
		};

		$this->category = $this->parseCategory1($result,0);

		$this->category->title = JText::_("COM_RXAFFILIATE_SEARCH_RESULT_FOR") . " " . $letter;

		return $this;
	}

	public function setSrch($srch,$page = 1){

		$result = $this->getSearch($srch,$page);

		$this->category = $this->parseCategory1($result,0);
		$this->category->title = JText::_("COM_RXAFFILIATE_SEARCH_RESULT_FOR") . " " . $letter;

		return $this;
	}

	public function getCategory(){
		return $this->category;
	}
}