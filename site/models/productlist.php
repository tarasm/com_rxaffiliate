<?php
/**
 * User: Taras
 * Date: 24.01.15
 * Time: 16:54
 */

include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelProductlist extends RxaffiliateModel {
	/** @var RxaffiliateTemplateProduct[] */
	protected $products = array();

	public function  setBestsellers(){
		$result = $this->getBestsellersList();

		foreach($result as $product){
			$this->products[] = $this->applyProduct($product);
		}

		return $this;
	}

	public function setFeatured(){
		$result = $this->getFeaturedList();

		foreach($result as $product){
			$this->products[] = $this->applyProduct($product);
		}

		return $this;
	}

	private function applyProduct($product){
		$prod = new RxaffiliateTemplateProduct();
		$prod->id = $product["product_id"];
		$prod->title = $product["product_title"];
		$prod->description = empty($product["product_description"]) ?
					$product["product_meta"] : $product["product_description"];
		$prod->quantity = $product["product_value"];
		$prod->weight = $product["product_weight"];
		$prod->price = $product["product_price"];
		$prod->imageUrl = $product["product_image"];
		$prod->imageWidth = $product["image_width"];
		$prod->imageHeight = $product["image_height"];

		return $prod;
	}

	public function getProducts(){
		return $this->products;
	}
}
