<?php
/**
 * User: taras
 * Date: 22.11.14
 * Time: 14:40
 */

class RxaffiliateTemplateCategory extends JObject {
	/** @var string */
	public $title;

	/** @var string */
	public $description;

	/** @var string */
	public $full_description;

	/** @var int */
	public $id;

	/** @var RxaffiliateTemplateCategory[] */
	public $subcategories = array();

	/** @var RxaffiliateTemplateProduct[] */
	public $products = array();

	/** @var string */
	public $meta_description;

	/** @var string */
	public $meta_keywords;

	/** @var RxaffiliateTemplateCategory */
	public $parent;

	/** @var  bool */
	public $active;
}