<?php
/**
 * User: Taras
 * Date: 13.01.15
 * Time: 19:30
 */
class RxaffiliateTemplateProduct extends JObject {
	/** @var  string */
	public $title;

	/** @var  int */
	public $id;

	/** @var  string */
	public $description;

	/** @var  string */
	public $full_description;

	/** @var  float */
	public $price;

	/** @var  string */
	public $quantity;

	/** @var  string */
	public $weight;

	/** @var  string */
	public $imageUrl;

	/** @var  int */
	public $imageWidth;

	/** @var  int */
	public $imageHeight;



	public function toArray(){
		$result = array();
		foreach( $this->getProperties(false) as $property => $value){
			if(is_scalar($value)){
				$result[$property] = $value;
			}
		}
		return $result;
	}
}