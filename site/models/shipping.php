<?php
/**
 * User: Taras
 * Date: 11.01.15
 * Time: 13:13
 */
include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelShipping extends RxaffiliateLibCarthelpers {
	public $price;
	public $term;

	public function __construct($config = array()){
		$result = xmlrpcClient::call(xmlrpcClient::SHIPPING);
		foreach($result as $key => $value){
			$this->set(str_replace("shipping_","",$key), $value);
		};

		parent::__construct($config);
	}

	public function getVal(){
		return $this->price;
	}
}