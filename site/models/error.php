<?php
/**
 * User: Taras
 * Date: 22.01.15
 * Time: 18:03
 */
include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelError extends RxaffiliateModel {

	/** @var  JSession */
	protected $session;

	/** @var  int */
	public $code;

	/** @var  string */
	public $message;

	public function __construct($config = array()) {
		$this->session = JFactory::getSession();

		if($this->session->get("error",null,"rxaffiliate")){
			$error = $this->session->get("error",null,"rxaffiliate");
			if(!empty($error["code"])){
				$this->code = $error["code"];
			}

			if(!empty($error["message"])){
				$this->message = $error["message"];
			}
		}

		parent::__construct($config);
	}


	public function setError($code = 0,$message = null){
		if($code == 0){
			$this->session->set("error",null,"rxaffiliate");
		}else{
			$this->session->set("error",array("code" => $code, "message" => $message),"rxaffiliate");
		}
	}


}