<?php
/**
 * User: Taras
 * Date: 13.01.15
 * Time: 8:20
 */
include_once(realpath( __DIR__ . "/../model.php"));

class RxaffiliateModelCatalog extends RxaffiliateModel {

	/** @var  RxaffiliateTemplateCategory[] */
	protected $categories;

	public function getCatalog(){
		if($this->categories){
			return $this->categories;
		}

		$result = xmlrpcClient::call(xmlrpcClient::VIEW_CATALOG);

		$this->categories = array();

		foreach($result as $category){
			$subcategory = array();
			foreach($category["subcategory_id"] as $key => $subcategory_id){
				$subcategory[] = new RxaffiliateTemplateCategory(array(
					"id" => $subcategory_id,
					"title" => $category["subcategory_title"][$key],
					"parent" => new RxaffiliateTemplateCategory(array(
							"id" => $category["category_id"],
							"title" => $category["category_title"]
						))
				));
			}

			$this->categories[] = new RxaffiliateTemplateCategory(array(
				"id" => $category["category_id"],
				"title" => $category["category_title"],
				"subcategories" => $subcategory
			));
		}
		return $this->categories;
	}
}