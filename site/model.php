<?php
/**
 * User: taras
 * Date: 05.10.14
 * Time: 14:51
 */
  require_once( __DIR__ . '/helpers/client_xmlrpc.php' );
  require_once(__DIR__ . "/models/templates/autoload.php");
  require_once(__DIR__ . "/models/lib/autoload.php");

  class RxaffiliateModel extends JModelLegacy {
	  protected $limit = 3;
      public $category_id = 0;
	  /**
	   * @var int
	   * peer page
	   */
	  public $pPage = 200;


	  public function __construct($config = array()){
		  parent::__construct($config);
	  }

	  public function getBestsellersList(){
		  $result = xmlrpcClient::call(xmlrpcClient::BESTSELLERSLIST,array($this->limit));
		  return $result;
	  }

	  public function  getFeaturedList(){
	      $result = xmlrpcClient::call(xmlrpcClient::FEATURED_PRODUCTS);
		  return $result;
      }

	  public function getCategoryList(){
	      $result = xmlrpcClient::call(xmlrpcClient::GET_CATEGORY_LIST);
          return $result;
      }

	  public function getCategory(){
          $result = xmlrpcClient::call(xmlrpcClient::VIEW_CATEGORY,array($this->category_id));
		  return $result;
	  }

	  public function getSubcategory(){
		  $reuslt = xmlrpcClient::call(xmlrpcClient::VIEW_SUBCATEGORY,array($this->category_id));
		  return $reuslt;
	  }

	  public function getPhones(){
		  $result = xmlrpcClient::call(xmlrpcClient::PHONES);
		  return $result;
	  }

	  protected function getSearchFirst($letter){
		  $result = xmlrpcClient::call(xmlrpcClient::SEARCH_FIRST,array($letter));
		  return $result;
	  }

	  protected function getSearch($srch,$page){
		  $result = xmlrpcClient::call(xmlrpcClient::SEARCH, array($srch,$page,$this->pPage));
		  return $result;
	  }

	  public function getBestsellers(){
		  $result = xmlrpcClient::call(xmlrpcClient::BESTSELLERSLIST);
		  return $result;
	  }

	  public function getCurrency(){
		  $params = JComponentHelper::getParams("com_rxaffiliate");
		  return $params->get("currency_symbol",'$');
	  }

	  public function getProduct($id){
		  $result = xmlrpcClient::call(xmlrpcClient::GET_PRODUCT,array($id));
		  $return = new JObject($result);
//		  $return->set("id",$result["id"]);
		  return $return;
	  }
  }
