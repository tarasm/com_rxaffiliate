<?php

  class RxaffiliateHelper {
	  public static $chat_script;

	  public static function productImageUrl($url){
         if($url == "images/default.jpg"){
	        return JUri::base(true) . "media/rxaffiliate/{$url}";
         }

		 return $url;
      }

	  public static function formatPrice($price){
		  $params = JComponentHelper::getParams('com_rxaffiliate');
          return sprintf("%01.2f %s",  $price, $params->get("currency_symbol"));
	  }

	  public static function printPrice($price){
		  print self::formatPrice($price);
	  }

	  public static function makeUrl(array $param){
		  if(empty($param['option'])){
			  $param['option'] = 'com_rxaffiliate';
		  }

		  foreach($param as $key => $val){
			  $param[$key] = $key . '=' . rawurlencode($val);
		  }

		  print JRoute::_( 'index.php?' . implode("&",$param) );
	  }

	  public static function getTitleFromActive(){
		  try{
		  	$menu = JFactory::getApplication()->getMenu();
		  	$active = $menu->getActive();
		  	return $active->title;
		  }catch (Exception $e){
			  return "";
		  }
	  }

	  public static function initChat(){
		  if(!self::$chat_script){
			  $params = JComponentHelper::getParams('com_rxaffiliate');
			  $chatUrl = $params->get("chat_url");

			  $chat_script = explode("/", JHtml::script("rxaffiliate/initchat.js",false,true,true));

			  $chat_script = implode(DIRECTORY_SEPARATOR,
				  array_splice( $chat_script,count(explode("/",JUri::root(true)))));

			  $chat_script = JPATH_ROOT . DIRECTORY_SEPARATOR . $chat_script;

			  $chat_script = str_replace("\$chat_url",$chatUrl, file_get_contents($chat_script));
			  $document = JFactory::getDocument();
			  $document->addScriptDeclaration($chat_script);

			  self::$chat_script = true;
		  }
	  }
  }