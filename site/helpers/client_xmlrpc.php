<?php
  include_once __DIR__ . "/phpxmlrpc/xmlrpc.php";

  class xmlrpcClient {

	  const CLEAR_CACHE2 = "func.clearCache2";
	  const IS_SHOP_CLOSED = "func.isShopClosed";
	  const SHIPPING = "func.Shipping";
	  const BESTSELLERSLIST = "func.getBestsellersList";
	  const GET_CATEGORY_LIST = "func.getCategoryList";
      const VIEW_CATEGORY = "func.viewCategory";
      const VIEW_CATALOG = "func.viewCatalog";
	  const VIEW_SUBCATEGORY = "func.viewSubCategory";
	  const VIEW_PRODUCT_INFO = "func.viewProductInfo";
	  const SEARCH_FIRST = "func.searchFirst";
	  const SEARCH = "func.search";
	  const GET_PRODUCT = "func.getProduct";
	  const GET_PRODUCT_INFO = "func.viewProductInfo";
      const GET_COUPON = "func.getCoupon";
	  const GET_DISCOUNTS = "func.getDiscounts";
	  const FEATURED_PRODUCTS = "func.featuredProducts";
	  const GET_PRODUCTS_LIST = "func.getProductsList";
      const GET_SUBCATEGORIES_LIST = "func.getSubcategoriesList";
      const GET_PAGES = "func.getPages";
      const GET_DRUGSTORE_BRANDS = "func.getDrugstoreBrands";
      const GET_DRUGSTORE_BRANDS_FULL = "func.getDrugstoreBrandsFull";
      const CLEAR_CACHE = "func.clearCache";
      const PHONES = "func.Phones";

	  static  $instance;

	  public static function getInstance(){
		  if(!self::$instance){
			  $params = JComponentHelper::getParams('com_rxaffiliate');
			  self::$instance = new xmlrpc_client($params->get('xmlrpc_url'));

              $host = JUri::getInstance()->getHost();
			  $config = new JConfig();

			  self::$instance->setCookie('partner', $params->get('partner'), '', $host);
			  //self::$instance->setCookie('template_id', $template_id, '', $host);
			  self::$instance->setCookie('unique_id', $params->get('unique_id'), '', $host);
			  self::$instance->setCookie('site_name', $config->sitename, '', $host);
			  self::$instance->setCookie('host_name', $host, '', $host);

//			  self::$instance->setCookie('XDEBUG_SESSION', 'phpstorm-xdebug', '', $host);

		  }
		  return self::$instance;
	  }

	  public static function _call_callback($name,$param = null){
		  if(is_array($param)){
			  foreach($param as $key => $value){
				  $param[$key] = php_xmlrpc_encode($value);
			  }
		  }else{
			  $param = array();
		  }

		  $reply = self::getInstance()->send(new xmlrpcmsg($name,$param));
		  return self::processResult( $reply->value() );
	  }

	  public static function call($name,$param = null){
		  $cache = JFactory::getCache("rxaffiliatecart");
		  /** @var JCacheControllerCallback $cache */
		  if($param){
			  return $cache->call(get_called_class() . "::_call_callback", $name, $param);
		  }else{
			  return $cache->call(get_called_class() . "::_call_callback", $name);
		  }
	  }

	  protected static function processResult($result){
           if(is_object($result)){
			   return self::processResult($result->getval());
		   }elseif(is_array($result)){
			   foreach($result as $key => $value){
				   $result[$key] = self::processResult($value);
			   }
			   return $result;
		   }else{
			   return $result;
		   }
	  }

  }