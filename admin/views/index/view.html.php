<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 04.10.14
 * Time: 15:53
 */

defined('_JEXEC') or die;

/**
 * View class for a list of redirection links.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_redirect
 * @since       1.6
 */
class RxaffiliateViewIndex extends JViewLegacy
{
	/**
	 * Display the view
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		$user	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_RXAFFILIATE_MANAGER_TITLE'), 'link weblinks');

		if ($user->authorise('core.admin', 'com_weblinks'))
		{
			JToolbarHelper::preferences('com_rxaffiliate');
		}
	}
}
