SET EXTENSION_NAME=rxaffiliate
SET JPATH_ROOT=D:\WebServers\home\partnernew\www

SET COMPONENT_NAME=com_%EXTENSION_NAME%
SET MODULE_NAME=mod_%EXTENSION_NAME%

@rem Make Languages
@mkdir language
@mkdir language\site
@mkdir language\site\en-GB
@mkdir language\admin
@mkdir language\admin\en-GB
mklink /H language\admin\en-GB\en-GB.%COMPONENT_NAME%.sys.ini %JPATH_ROOT%\administrator\language\en-GB\en-GB.%COMPONENT_NAME%.sys.ini
mklink /H language\admin\en-GB\en-GB.%COMPONENT_NAME%.ini     %JPATH_ROOT%\administrator\language\en-GB\en-GB.%COMPONENT_NAME%.ini
mklink /H language\site\en-GB\en-GB.%COMPONENT_NAME%.ini     %JPATH_ROOT%\language\en-GB\en-GB.%COMPONENT_NAME%.ini
@rem ==============

@rem Make media
mklink /J media %JPATH_ROOT%\media\%EXTENSION_NAME%\
@rem Make administrator folder
mklink /J admin %JPATH_ROOT%\administrator\components\%COMPONENT_NAME%\
mklink /H %EXTENSION_NAME%.xml %JPATH_ROOT%\administrator\components\%COMPONENT_NAME%\%EXTENSION_NAME%.xml
@rem Make sql folder
mklink /J sql %JPATH_ROOT%\administrator\components\%COMPONENT_NAME%\sql\
@rem Make site folder
mklink /J  site %JPATH_ROOT%\components\%COMPONENT_NAME%\

@rem Add module
mkdir modules
mklink /J modules\%MODULE_NAME%\ %JPATH_ROOT%\modules\%MODULE_NAME%
mklink /H modules\%MODULE_NAME%\en-GB.%MODULE_NAME%.ini     %JPATH_ROOT%\language\en-GB\en-GB.%MODULE_NAME%.ini
mklink /H modules\%MODULE_NAME%\en-GB.%MODULE_NAME%.sys.ini     %JPATH_ROOT%\language\en-GB\en-GB.%MODULE_NAME%.sys.ini
