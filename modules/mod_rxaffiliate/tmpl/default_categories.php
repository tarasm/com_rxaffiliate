<?php
/**
 */

/** @var RxaffiliateModelCatalog $model */
$model = ModRxaffiliateHelper::getModel("catalog");

$categories = $model->getCatalog();


foreach($categories as $category){
  ?><a href="<?php ModRxaffiliateHelper::makeUrl(array('view' => 'category'
                     , 'id' => $category->id)); ?>" class="mod-rxaffiliate-category-item<?php
			echo(ModRxaffiliateHelper::isActiveCategory($category) ? " active" : ""); ?>"><?php
			echo($category->title); ?></a>
<?php } ?>