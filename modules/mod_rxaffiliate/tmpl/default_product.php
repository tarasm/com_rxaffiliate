<?php
/**
 * product template
 * input $product
 */

/** @var RxaffiliateTemplateProduct $product */

?><div class="mod-rxaffiliate-product-thumbnail">
	<div class="mod-rxaffiliate-product-thumbnail-tooltip">
		<div class="mod-rxaffiliate-product-thumbnail-tooltip-labels">
			<?php echo(JText::_("COM_RXAFFILIATE_WEIGHT")); ?>: <?php echo($product->weight); ?><br>
			<?php echo(JText::_("COM_RXAFFILIATE_SET")); ?>: <?php echo($product->quantity); ?><br>
			<?php echo(JText::_("COM_RXAFFILIATE_OUR_PRICE")); ?>: <?php
						ModRxaffiliateHelper::printPrice($product->price); ?><br>
			<br>
			<a href="<?php ModRxaffiliateHelper::makeUrl(array(
					"task" => "addproduct", "id" => $product->id));
				?>" class="mod-rxaffiliate-product-thumbnail-add-to-cart btn"><?php
					echo(JText::_("COM_RXAFFILIATE_ADDCART")); ?></a>
		</div>
	</div>
	<a href="<?php ModRxaffiliateHelper::makeUrl(array(
		"view" => "product", "id" => $product->id)); ?>" class="mod-rxaffiliate-product-thumbnail-name"><?php
			echo($product->title); ?></a>
	<img alt="<?php	echo($product->title); ?>" src="<?php echo($product->imageUrl); ?>">
</div>
