<?php
/**
 * User: Taras
 * Date: 24.01.15
 * Time: 23:23
 */

/** @var RxaffiliateModelProductlist $model */
$model = ModRxaffiliateHelper::getModel("productlist");
$model->setFeatured();

foreach($model->getProducts() as $product){
	include(__DIR__ . "/default_product.php");
}

