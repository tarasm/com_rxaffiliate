<?php
/**
 */
defined('_JEXEC') or die; ?>

<form name="search_frm" method="post" id="search_frm" action="<?php
          ModRxaffiliateHelper::makeUrl(array("task" => "search")) ?>">
	<input type="text" autocomplete="off" id="Suggest" name="srch" value="<?php
			echo($input->getString("srch","")); ?>" style="width: auto; display: inline;" />
	<input type="submit" value="S" class="icon-search"
		   style="font-family: IcoMoon; height: auto; width: auto; padding: 2px 6px;" />
</form>

<?php   if($params->get("alphabet",1)){ ?>
  <div class="search-alphabet">
<?php     for( $letter = 'A'; $letter !== 'AA'; ++$letter){ ?>
			  <a href="<?php ModRxaffiliateHelper::makeUrl(array(
				      "task"=> "searchfirst", "srch" => $letter ));
			     ?>"><?php echo $letter; ?></a>
<?php     };  ?>
  </div>
<?php   }  ?>