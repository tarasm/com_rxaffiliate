<?php
/**
 * User: Taras
 * Date: 09.01.15
 * Time: 9:34
 */

/** @var RxaffiliateModelProductlist $model */
$model = ModRxaffiliateHelper::getModel("productlist");
$model->setBestsellers();
$bestseller_img = JHtml::image("rxaffiliate/bestsellers.png","bestsellers",null,true);

?>
<div id="mod-rxaffiliate-bestsellers">
	<div class="mod-rxaffiliate-bestsellers-icon-title">
		<?php echo $bestseller_img; ?>
	</div>
<?php
foreach($model->getProducts() as $product){
	include(__DIR__ . "/default_product.php");

//	include( JModuleHelper::getLayoutPath('mod_rxaffiliate',
//		$params->get('layout', 'default') . "_product"));
}
?></div>
