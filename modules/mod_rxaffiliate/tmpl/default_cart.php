<?php
/**
 * User: Taras
 * Date: 25.01.15
 * Time: 0:12
 */

/** @var RxaffiliateModelCart $model */
$model = ModRxaffiliateHelper::getModel("cart");
$model->calculate();
?>
<div class="rxaffiliate-mod-cart">
	<span><?php echo(JText::_("COM_RXAFFILIATE_SHOPPING_CART")); ?></span>
<?php if(count($model->products) > 0){ ?>
	<b><?php ModRxaffiliateHelper::printPrice($model->total); ?></b>
	<a href="<?php ModRxaffiliateHelper::makeUrl(array("view" => "cart")); ?>"><?php
		echo(count($model->products)); ?> <?php
		echo(JText::_("COM_RXAFFILIATE_ITEM"));	?></a>
<?php
      }else{
      	echo(JText::_("COM_RXAFFILIATE_EMPTY"));
      } ?>
</div>
