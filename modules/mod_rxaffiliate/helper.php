<?php
/**
 * @subpackage  mod_rxaffiliate
 *
 */

defined('_JEXEC') or die;

if(!defined("COM_RXAFFILIATE_COMPONENT_PATH")) {
	define("COM_RXAFFILIATE_COMPONENT_PATH",JPATH_SITE . DIRECTORY_SEPARATOR . "components" .
			DIRECTORY_SEPARATOR . "com_rxaffiliate");
};

//include_once(JPATH_SITE . "/components/com_rxaffiliate/model.php");
JModelLegacy::addIncludePath(JPATH_SITE . "/components/com_rxaffiliate/model.php","RxaffiliateModel");

include_once(COM_RXAFFILIATE_COMPONENT_PATH . "/helpers/helper.php");
/**
 * Helper model for mod_rxaffiliate
 *
 * @subpackage  mod_rxaffiliate
 */
class ModRxaffiliateHelper extends RxaffiliateHelper{
    protected static $model = array();

	/**
	 * @return RxaffiliateModel
	 */
	public static function getModel($name = "default"){
	  if(!self::$model[$name] ){
		  JModelLegacy::addIncludePath(JPATH_ROOT . '/components/com_rxaffiliate/models', 'RxaffiliateModel');
		  self::$model[$name] = JModelLegacy::getInstance($name,"RxaffiliateModel");
      }
	  return self::$model[$name];
    }

	public static function getCategoryList(){
        $categoryList = self::getModel()->getCategoryList();
		return $categoryList;
	}

	/**
	 * @param RxaffiliateTemplateCategory $category
	 * @return bool
	 */
	public static function isActiveCategory(RxaffiliateTemplateCategory $category){
		$input = JFactory::getApplication()->input;

		if($input->getWord("option","") != "com_rxaffiliate"){
			return false;
		}

		switch($input->getWord("view","")){
			case "category":
				if(($input->getInt("id",-1) == $category->id) || ($input->getInt("id",-1) == $category->parent->id)){
					return true;
				}

				if(is_array($category->subcategories)){
					foreach($category->subcategories as $subcategory){
						if($input->getInt("id",-1) == $subcategory->id){
							return true;
						}
					}
				}
				break;
		}

		return false;
	}

	public static function getCountryClassFromName($country){
		$country_code = null;
		switch($country){
			case "UK": $country_code = "uk"; break;
			case "USA": $country_code = "us"; break;
			case "FRANCE": $country_code = "fr"; break;
			case "BELGIUM": $country_code = "be"; break;
			case "ITALY": $country_code = "it"; break;
			case "SPAIN": $country_code = "es"; break;
			case "AUSTRALIA": $country_code = "au"; break;
		}

		if($country_code){
			return "mod-rxaffiliate-flag-" . $country_code;
		}else{
			return null;
		}
	}
}