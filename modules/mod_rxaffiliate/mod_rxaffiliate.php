<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the login functions only once
require_once __DIR__ . '/helper.php';

JHtml::stylesheet("rxaffiliate/mod.css",array(),true);

$input = JFactory::getApplication()->input;

JFactory::getLanguage()->load("com_rxaffiliate");

require JModuleHelper::getLayoutPath('mod_rxaffiliate',
	    $params->get('layout', 'default') . "_"
	  . $params->get('content','phone'));
